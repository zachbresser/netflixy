import { createStore, applyMiddleware } from "redux";
import { persistStore } from "redux-persist";
import rootReducer from "../reducers";
import toastMiddleware from "../middleware/toasts";
import api from "../middleware/api";

const configureStore = initialState => {
  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(api, toastMiddleware)
  );
  const persiststore = persistStore(store);
  return { store, persiststore };
};

export default configureStore;
