import { createStore, applyMiddleware, compose } from "redux";
import { persistStore } from "redux-persist";
import rootReducer from "../reducers";
import logger from "redux-logger";
import toastMiddleware from "../middleware/toasts";
import api from "../middleware/api";
import DevTools from "../containers/DevTools";

const configureStore = initialState => {
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(logger, api, toastMiddleware),
      DevTools.instrument()
    )
  );
  if (module.hot) {
    // Enable webpack hot module replacement for reducers
    module.hot.accept("../reducers", () => {
      store.replaceReducer(rootReducer);
    });
  }
  const persiststore = persistStore(store);
  return { store, persiststore };
};

export default configureStore;
