import React from "react";
import HelpMenu from "../components/HelpMenu";
import { connect } from "react-redux";
import { showToast } from "../actions/toastActions";

const HelpMenuContainer = props => <HelpMenu {...props} />;

export default connect(
  null,
  { showToast }
)(HelpMenuContainer);
