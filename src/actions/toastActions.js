import { SHOW_TOAST } from "../constants/actionTypes";

export const showToast = (type, message) => {
  console.log("I am in showtoast with these props: ", type, message);
  return {
    type: SHOW_TOAST,
    payload: {
      message,
      type
    }
  };
};
