import React from 'react';
import ReactDOM from 'react-dom';
import Root from "./containers/Root";
import registerServiceWorker from './registerServiceWorker';
import injectGlobalStyles from "./injectGlobalStyles";

ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
injectGlobalStyles();
