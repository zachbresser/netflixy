import { handleActions } from "redux-actions";
import { GET_MOVIES, SET_MOVIES } from "../constants/actionTypes";

export default handleActions(
  {
    [GET_MOVIES]: (state, action) => state,
    [SET_MOVIES]: (state, action) => action.payload
  },
  {}
);
