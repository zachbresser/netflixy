import React from "react";

const FontAwesomeIcon = ({ icon = "ellipsis-h", text = false, onClick }) => (
  <div>
    <span onClick={onClick} className={`fas fa-${icon}`} />
    {Boolean(text) && <span> &nbsp; {text}</span>}
  </div>
);

export default FontAwesomeIcon;
